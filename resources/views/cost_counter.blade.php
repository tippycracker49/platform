@extends('layouts.website')

@section('content')
    <header class="masthead">
        <div class="container h-100">
            <div class="row h-100">
            <div class="col-lg-7 my-auto">
                <div class="header-content mx-auto">
                <h1 class="mb-3">Pasiskaičiuokite norimo projekto kainą su mūsų skaičiuoklę</h1>
                <p class="mb-4">Modern products for sales, marketing and support to connect with customers and grow faster.</p>
                <a href="#download" class="btn btn-outline btn-xl js-scroll-trigger">Sužinokite daugiau!</a>
                </div>
            </div>
            </div>
        </div>
    </header>
    <cost-counter></cost-counter>
@endsection