@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Clients</div>
                <div class="col-sm-12">
                        @if($clients->count() > 0)
                            <table class="table">
                                <thead class="thead-gradient">
                                    <tr>
                                        <th scope="col">Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Phone</th>
                                        <th scope="col">Created At</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($clients as $client)
                                        <tr>
                                            <th>{{ $client->name }}</th>
                                            <td>{{ $client->email }}</td>
                                            <td>{{ $client->phone }}</td>
                                            <td>{{ $client->created_at }}</td>
                                         </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <h3 class="empty-table">No clients yet.</h3>
                        @endif
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
