@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Projects</div>
                <div class="col-sm-12">
                        @if($projects->count() > 0)
                            <table class="table">
                                <thead class="thead-gradient">
                                    <tr>
                                        <th scope="col">Title</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Created At</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($projects as $project)
                                        <tr>
                                            <th>{{ $project->title }}</th>
                                            <td>{{ $project->description }}</td>
                                            <td>{{ $project->created_at }}</td>
                                         </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <h3 class="empty-table">No propjects yet.</h3>
                        @endif
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
