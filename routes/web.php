<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('website');
})->name('home');
Route::get('/skaiciuokle', function () {
    return view('cost_counter');
})->name('cost-counter');


Auth::routes();

Route::get('/projects', 'ProjectController@index')->name('projects');
Route::get('/clients', 'ClientController@index')->name('clients');
